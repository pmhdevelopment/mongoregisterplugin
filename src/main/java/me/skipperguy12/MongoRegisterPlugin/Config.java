package me.skipperguy12.MongoRegisterPlugin;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

/**
 * Created by neiljohari on 2/3/14.
 */
public class Config {
    protected static File configFile;
    private static FileConfiguration config;
    static {
        configFile = new File(MongoRegisterPlugin.get().getDataFolder(), "config.yml");
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    public static <T> T get(String path) {
        return (T) config.get(path);
    }

    public static <T> T get(String path, Object def) {
        return (T) config.get(path, def);
    }


    public static void set(String path, Object value) {
        config.set(path, value);
        try {
            config.save(configFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String site = get("site", "127.0.0.1");
    public static String port = get("site-port", "30000");

    public static class Email {
        public static String successMessage = get("email.success-message", ChatColor.GREEN + "Please check your email for a confirmation link! Thanks for joining us, have a great day!");
        public static String emailContents = get("email.email-contents", "Click right here to activate your account: %s");
        public static String emailSubject = get("email.email-subject", "Activate your account!");
        public static String senderEmail = get("email.sender-email");
        public static String senderPassword = get("email.sender-password");
    }

    public static class Database {
        public static String usersCollectionName = get("database.users-collection", "users");
    }

}
