package me.skipperguy12.MongoRegisterPlugin.commands;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.sk89q.minecraft.util.commands.Command;
import com.sk89q.minecraft.util.commands.CommandContext;
import com.sk89q.minecraft.util.commands.CommandException;
import me.skipperguy12.MongoRegisterPlugin.Config;
import me.skipperguy12.MongoRegisterPlugin.database.CollectionQuery;
import me.skipperguy12.MongoRegisterPlugin.database.DatabaseUtils;
import me.skipperguy12.MongoRegisterPlugin.events.PlayerRegisterEvent;
import me.skipperguy12.MongoRegisterPlugin.utils.DBO;
import me.skipperguy12.MongoRegisterPlugin.utils.GoogleMail;
import me.skipperguy12.MongoRegisterPlugin.utils.HTTPConnectionUtil;
import me.skipperguy12.MongoRegisterPlugin.utils.UsernameUUIDUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Pattern;

import static me.skipperguy12.MongoRegisterPlugin.utils.HTTPConnectionUtil.getFormattedTargetURL;

/**
 * Created by neiljohari on 2/3/14.
 */
public class RegisterCommand {
    private static final Pattern rfc2822 = Pattern.compile(
            "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
    );


    @Command(aliases = { "register", "reg" }, desc = "Registers yourself on site!", usage = "[email] [password] [confirmation]", min = 3, max = 3)
    public static void register(final CommandContext args, CommandSender sender) throws Exception {
        if(!(sender instanceof Player))
            throw new CommandException("You must be a Player to register on site!");

        Player player = (Player) sender;
        String uuid = UsernameUUIDUtils.getUUIDFromUsername(player.getName());
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("uuid", uuid);
        parameters.put("email", args.getString(0));
        parameters.put("hash", "test");
        parameters.put("password", args.getString(1));
        parameters.put("confirmation", args.getString(2));


        // Check if user already registered
        DBObject existingTest = (DBObject) CollectionQuery.executeQuery(CollectionQuery.buildQuery(DatabaseUtils.users, CollectionQuery.QueryType.FINDONE, new BasicDBObject("UUID", parameters.get("uuid"))));
        if (existingTest != null){
            if(existingTest.get("encrypted_password") != null && existingTest.get("encrypted_password") != "")
                throw new CommandException("You're already registered!");
        }
        // Verify the input has no harmful elements in it
        String test = parameters.get("uuid") + parameters.get("email") + parameters.get("hash") + parameters.get("password");
        if (test.contains("\'") || test.contains("\\") || test.contains("\"")){
            System.out.println("Invalid characters! Please do not use \', \", or \\.");
        }

        // Email confirmation
        if(!rfc2822.matcher(parameters.get("email")).matches())
            throw new CommandException("Please provide a valid email, you need it to receive our confirmation email!");

        // Passwords
        if(!parameters.get("password").equals(parameters.get("confirmation")))
            throw new CommandException("Your passwords must match!");

        if(parameters.get("password").toString().length() < 8)
            throw new CommandException("Your password must be greater than 8 characters! We wouldn't want your account getting stolen, now would we?");


        Bukkit.getPluginManager().callEvent(new PlayerRegisterEvent(player, parameters));
    }
}
