package me.skipperguy12.MongoRegisterPlugin;

import com.sk89q.bukkit.util.CommandsManagerRegistration;
import com.sk89q.minecraft.util.commands.*;
import me.skipperguy12.MongoRegisterPlugin.database.DatabaseUtils;
import me.skipperguy12.MongoRegisterPlugin.listeners.PlayerConnectionListener;
import me.skipperguy12.MongoRegisterPlugin.listeners.PlayerRegisterListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * Created by neiljohari on 2/3/14.
 */
public class MongoRegisterPlugin extends JavaPlugin {
    private CommandsManager<CommandSender> commands;
    private static MongoRegisterPlugin instance;

    public static MongoRegisterPlugin get() {
        if(instance == null) {
            instance = new MongoRegisterPlugin();
        }

        return instance;
    }

    public void onEnable() {
        instance = this;

        if(!getDataFolder().exists() || !new File(getDataFolder(), "config.yml").exists())
            this.saveDefaultConfig();

        setupCommands();
        registerListeners();
        setupDatabase();
    }


    public void onDisable() {
        instance = null;
    }

    // Registers all the commands
    private void setupCommands() {
        this.commands = new CommandsManager<CommandSender>() {
            @Override
            public boolean hasPermission(CommandSender sender, String perm) {
                return sender instanceof ConsoleCommandSender || sender.hasPermission(perm);
            }
        };
        CommandsManagerRegistration cmdRegister = new CommandsManagerRegistration(this, this.commands);
        // TODO: Register some commands here
    }

    private void registerEvents(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, this);
    }

    private void registerListeners() {
        registerEvents(new PlayerConnectionListener());
        registerEvents(new PlayerRegisterListener());
    }

    private void setupDatabase() {
        DatabaseUtils.users = DatabaseUtils.getDB().getCollection(Config.Database.usersCollectionName);
    }


    /**
     * Called when a player or the console executes a command. Passes it to the
     * framework
     */
    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String commandLabel, String[] args) {
        try {
            this.commands.execute(cmd.getName(), args, sender, sender);
        } catch (CommandPermissionsException e) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to do that!");
        } catch (MissingNestedCommandException e) {
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (CommandUsageException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
            sender.sendMessage(ChatColor.RED + e.getUsage());
        } catch (WrappedCommandException e) {
            if (e.getCause() instanceof NumberFormatException) {
                sender.sendMessage(ChatColor.RED + "Something unexpected happened, we wanted an Integer, like 1 or 2, we received some text!");
            } else if (e.getCause() instanceof IllegalArgumentException) {
                sender.sendMessage(ChatColor.RED + e.getMessage().replace("java.lang.IllegalArgumentException: ", ""));
            } else {
                sender.sendMessage(ChatColor.RED + "We're not quite sure what happened! If you believe this is a problem that must be addressed, contact an administrator!");
                e.printStackTrace();
            }
        } catch (CommandException e) {
            sender.sendMessage(ChatColor.RED + e.getMessage());
        }
        return true;
    }
}
