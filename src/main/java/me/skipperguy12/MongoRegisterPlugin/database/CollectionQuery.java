package me.skipperguy12.MongoRegisterPlugin.database;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import javax.annotation.Nullable;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by neiljohari on 2/3/14.
 */
public class CollectionQuery {
    public static enum QueryType {
        COUNT, FIND, FINDONE, INSERT, REMOVE, SAVE, UPDATE;
    }
    public static class Query extends CancellableRunnable {
        private DBCollection collection;
        private QueryType type;
        private DBObject query;
        private Object result;
        private final AtomicBoolean complete = new AtomicBoolean(false);

        private @Nullable
        DBObject newQuery;
        /**
         * Constructor for SkipperRunnable
         *
         * @param taskID id to later cancel the Runnable, obtained while scheduling tasks
         */
        public Query(int taskID, DBCollection collection, QueryType type, DBObject query) {
            super(taskID);
            this.type = type;
            this.query = query;
            this.collection = collection;
        }

        public Query(int taskID, DBCollection collection, QueryType type) {
            super(taskID);
            this.type = type;
            this.collection = collection;
        }



        public Query(int taskID, DBCollection collection, QueryType type, DBObject query, DBObject newQuery) {
            super(taskID);
            this.type = type;
            this.query = query;
            this.collection = collection;
            this.newQuery = newQuery;
        }



        @Override
        public void run() {
            if(alive) {
                if(newQuery == null){
                    switch(type) {
                        case COUNT:
                            result = collection.count(query);
                            completeAndCancel();
                            break;

                        case FIND:
                            result = collection.find(query);
                            completeAndCancel();
                            break;

                        case FINDONE:
                            result = collection.findOne(query);
                            completeAndCancel();
                            break;

                        case INSERT:
                            result = collection.insert(query);
                            completeAndCancel();
                            break;

                        case REMOVE:
                            result = collection.remove(query);
                            completeAndCancel();
                            break;

                        case SAVE:
                            result = collection.save(query);
                            completeAndCancel();
                            break;


                        default:
                            cancel();
                            break;
                    }
                } else {
                    switch(type) {
                        case UPDATE:
                            result = collection.update(query, newQuery);
                            completeAndCancel();
                            break;
                        default:
                            completeAndCancel();
                            break;
                    }
                }
            }
        }

        private void completeAndCancel(){
            complete.set(true);
            cancel();
        }

        public boolean isComplete() {
            return complete.get();
        }

        public Object getResult() {
            return result;
        }
    }

    public static Query buildQuery(DBCollection collection, QueryType type, DBObject query, DBObject newQuery) {
        int taskId = 0;
        Query task = new Query(taskId, collection, type, query, newQuery);
        return task;
    }


    public static Query buildQuery(DBCollection collection, QueryType type, DBObject query) {
        int taskId = 0;
        Query task = new Query(taskId, collection, type, query);
        return task;
    }

    public static Query buildQuery(DBCollection collection, QueryType type) {
        int taskId = 0;
        Query task = new Query(taskId, collection, type);
        return task;
    }

    public static Object executeQuery(Query task){
        new Thread(task).start();
        while(!task.isComplete()){}
        return task.getResult();
    }
}