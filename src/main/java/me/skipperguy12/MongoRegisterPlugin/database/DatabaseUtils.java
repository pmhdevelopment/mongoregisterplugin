package me.skipperguy12.MongoRegisterPlugin.database;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import me.skipperguy12.MongoRegisterPlugin.Config;
import me.skipperguy12.database.plugin.Database;

/**
 * Created by neiljohari on 2/3/14.
 */
public class DatabaseUtils {

    public static DBCollection users;

    public static DB getDB() {
        return Database.getConnectionManager().getClient().getDB(Database.getConnectionManager().getDatabaseContext().getDB());
    }
}
