package me.skipperguy12.MongoRegisterPlugin.utils;

public class InvalidConversionJobException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -829038235886737871L;

    public InvalidConversionJobException() {
	super();
    }

    public InvalidConversionJobException(String message) {
	super(message);
    }

    public InvalidConversionJobException(String message, Throwable cause) {
	super(message, cause);
    }

    public InvalidConversionJobException(Throwable cause) {
	super(cause);
    }
}
