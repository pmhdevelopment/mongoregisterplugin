package me.skipperguy12.MongoRegisterPlugin.utils;

import com.google.common.collect.Lists;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import me.skipperguy12.MongoRegisterPlugin.database.CollectionQuery;
import me.skipperguy12.MongoRegisterPlugin.database.DatabaseUtils;

import java.util.Collection;

public class UsernameUUIDUtils {
    public static String getLastNameUsedFromUUID(String uuid){
      Collection<String> usernames = getUsernamesFromUUID(uuid);
      if(usernames == null || usernames.isEmpty())
          return "";
      return usernames.toArray()[usernames.size()-1].toString();
    }

    /**
     * Gets all the logged usernames that the user with the specified UUID has
     * logged in with
     * 
     * @param uuid
     *            The users UUID
     * @return Collection of names used by this user
     */
    public static Collection<String> getUsernamesFromUUID(String uuid) {
        Collection<String> retVal = Lists.newArrayList();
        BasicDBObject query = new BasicDBObject();
        query.put("UUID", uuid);
        DBObject user = (DBObject) CollectionQuery.executeQuery(CollectionQuery.buildQuery(DatabaseUtils.users, CollectionQuery.QueryType.FINDONE));
        if(user == null)
            return retVal;

        Collection<String> tempRet = DBO.getStringList(user, "names");

        if(tempRet != null && !tempRet.isEmpty())
            return  tempRet;

        return retVal;
    }

    /**
     * Gets the specified user's UUID from Mojang's API
     * 
     * @param username
     *            String username of the user
     * @return String uuid of the user
     * @throws InvalidConversionJobException
     *             Thrown if username is invalid or if Mojang's API fails
     */
    public static String getUUIDFromUsername(String username) throws InvalidConversionJobException {
        ConversionJob job = new ConversionJob(username);
        job.run();
        if (job.getUsername() == null || job.getUsername() == "")
            throw new InvalidConversionJobException("Username cannot be null or blank");
        if (job.getUUID() == null || job.getUUID() == "")
            throw new InvalidConversionJobException("UUID cannot be null or blank. User " + job.getUsername() + " may not be a valid Minecraft username!");

        return job.getUUID();
    }
}
