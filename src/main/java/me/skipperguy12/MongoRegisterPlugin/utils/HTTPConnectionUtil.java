package me.skipperguy12.MongoRegisterPlugin.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by neiljohari on 12/25/13.
 */
public class HTTPConnectionUtil {
    public   static String sendPost(String targetURL, HashMap<String, String> parameters) throws IOException {
        String connectURL = getFormattedTargetURL(targetURL, parameters).toString();
        System.out.println("Attempting to open connection to " + connectURL);
        URL url = new URL(connectURL);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.connect();
        return con.getResponseMessage();
    }

    public static StringBuilder getFormattedTargetURL(String targetUrl, HashMap<String, String> parameters){
        StringBuilder parametersURL = new StringBuilder();
        parametersURL.append(targetUrl + "?");
        int n = 0;
        for(Map.Entry<String, String> entry : parameters.entrySet()){
            parametersURL.append((n > 0 ? "&" : "") + entry.getKey() + "=" + entry.getValue());
            n++;
        }
        return parametersURL;
    }
}
