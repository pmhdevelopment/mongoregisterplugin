package me.skipperguy12.MongoRegisterPlugin.events;

import me.skipperguy12.MongoRegisterPlugin.MongoRegisterPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import java.util.HashMap;

/**
 * Created by neiljohari on 2/3/14.
 */
public class PlayerRegisterEvent extends MongoRegisterPluginEvent {
    static final HandlerList handlers = new HandlerList();

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private Player player;
    private HashMap<String, String> parameters = new HashMap<String, String>();

    public PlayerRegisterEvent(Player player, HashMap<String, String> parameters) {
        this.player = player;
        this.parameters = parameters;
    }

    public Player getPlayer() {
        return player;
    }

    public HashMap<String, String> getParameters() {
        return parameters;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}
