package me.skipperguy12.MongoRegisterPlugin.listeners;

import com.mongodb.BasicDBObject;
import me.skipperguy12.MongoRegisterPlugin.database.CollectionQuery;
import me.skipperguy12.MongoRegisterPlugin.database.DatabaseUtils;
import me.skipperguy12.MongoRegisterPlugin.utils.InvalidConversionJobException;
import me.skipperguy12.MongoRegisterPlugin.utils.MongoUtils;
import me.skipperguy12.MongoRegisterPlugin.utils.UsernameUUIDUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by neiljohari on 2/3/14.
 */
public class PlayerConnectionListener implements Listener {
    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        BasicDBObject query = new BasicDBObject();
        String userUUID = null;

        try {
            userUUID = UsernameUUIDUtils.getUUIDFromUsername(player.getName());
            query.put("UUID", userUUID);
        } catch (InvalidConversionJobException ex) {
            ex.printStackTrace();
            return;
        }

        if (!(DatabaseUtils.users.count(query) > 0)) {
            // Create new user
            System.out.println("Creating new document for " + player.getName() + " in database!");
            BasicDBObject newUser = new BasicDBObject();
            newUser.put("UUID", userUUID);
            newUser.put("names", Arrays.asList(player.getName()));
            newUser.put("first_login", new Date());
            CollectionQuery.executeQuery(CollectionQuery.buildQuery(DatabaseUtils.users, CollectionQuery.QueryType.INSERT, newUser));
        }

        CollectionQuery.executeQuery(CollectionQuery.buildQuery(DatabaseUtils.users, CollectionQuery.QueryType.UPDATE, query, MongoUtils.setKeyValue("last_login", new Date())));


    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent e){
        Player player = e.getPlayer();
        BasicDBObject query = new BasicDBObject();
        String userUUID = null;

        try {
            userUUID = UsernameUUIDUtils.getUUIDFromUsername(player.getName());
            query.put("UUID", userUUID);
        } catch (InvalidConversionJobException ex) {
            ex.printStackTrace();
            return;
        }

        CollectionQuery.executeQuery(CollectionQuery.buildQuery(DatabaseUtils.users, CollectionQuery.QueryType.UPDATE, query, MongoUtils.setKeyValue("last_logout", new Date())));


    }
}
