package me.skipperguy12.MongoRegisterPlugin.listeners;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import me.skipperguy12.MongoRegisterPlugin.Config;
import me.skipperguy12.MongoRegisterPlugin.database.CollectionQuery;
import me.skipperguy12.MongoRegisterPlugin.database.DatabaseUtils;
import me.skipperguy12.MongoRegisterPlugin.events.PlayerRegisterEvent;
import me.skipperguy12.MongoRegisterPlugin.utils.DBO;
import me.skipperguy12.MongoRegisterPlugin.utils.GoogleMail;
import me.skipperguy12.MongoRegisterPlugin.utils.HTTPConnectionUtil;
import org.apache.commons.lang.Validate;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;

import static me.skipperguy12.MongoRegisterPlugin.utils.HTTPConnectionUtil.getFormattedTargetURL;

/**
 * Created by neiljohari on 2/3/14.
 */
public class PlayerRegisterListener implements Listener {
    @EventHandler
    public void onPlayerRegisterEvent(PlayerRegisterEvent event) {

        String resp = null;
        try {
            resp = HTTPConnectionUtil.sendPost("http://\" + Config.site +\":\" + Config.port  +\"/key/new", event.getParameters());
        } catch (IOException e) {
            event.getPlayer().sendMessage("Unable to connect try again later!");
        }

        if(!resp.equalsIgnoreCase("ok")){
            event.getPlayer().sendMessage("Unable to connect try again later!");
            return;
        }



        try {
            event.getPlayer().sendMessage(Config.Email.successMessage);

            HashMap<String, String> parametersForRegister = new HashMap<String, String>();
            DBObject keyForUser = (DBObject) CollectionQuery.executeQuery(CollectionQuery.buildQuery(DatabaseUtils.users, CollectionQuery.QueryType.FINDONE, new BasicDBObject("UUID", event.getParameters().get("uuid"))));
            Validate.notNull(parametersForRegister, "Registration Link Params");
            Validate.notNull(keyForUser, "Key DBObject from Database");
            Validate.notNull(keyForUser.get("key"), "Key value in Database, query: " + keyForUser);
            parametersForRegister.put("key", DBO.getString(keyForUser, "key"));
            GoogleMail.Send(Config.Email.senderEmail, Config.Email.senderPassword, event.getParameters().get("email"), Config.Email.emailSubject, String.format(Config.Email.emailContents, getFormattedTargetURL("http://" + Config.site + ":" + Config.port + "/register/register_user", parametersForRegister)));

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
